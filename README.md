# Lytn - Predictive Analytics for SD-WAN



## Getting started

Device Template for Lytn SD-WAN Capacity & Quality Management Platform

This project includes a PRTG custom device template and a corresponding lookup file for Lytn’s predicitive SD WAN capacity and quality-of-experience management platform.

More details about the Lytn system, and the data available through the device template, can be found in this blog article - https://blog.paessler.com/lytn-and-prtg-a-crystal-ball-for-your-sd-wan


To use the template, simply copy the Lytd.odt file to the PRTG template folder (default - C:\Program Files (x86)\PRTG Network Monitor\devicetemplates), and the Lytn_LQS.ovl Lookup file to the custom lookup folder (default - C:\Program Files (x86)\PRTG Network Monitor\lookups\custom). Don’t forget to refresh the lookup list (Setup – System Administration – Administrative Tools – Load Lookups & File Lists). 

